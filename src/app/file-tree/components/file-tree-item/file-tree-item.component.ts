import { Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { takeUntil, tap } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { Subject } from 'rxjs';

import { FileTreeModel } from '../../../shared/models';
import { CommentDialogComponent } from '../../../shared/dialogs/comment-dialog/comment-dialog.component';

@Component({
  selector: 'file-tree-item',
  templateUrl: './file-tree-item.component.html',
  styleUrls: ['./file-tree-item.component.scss']
})
export class FileTreeItemComponent implements OnDestroy {
  @Input() fileTreeItem: FileTreeModel;

  @Output() commentChangedEvent = new EventEmitter<FileTreeModel>();

  destroy$ = new Subject();

  constructor(private dialog: MatDialog) { }

  openCommentDialog() {
    this.dialog.open(CommentDialogComponent, {
      width: '50vw',
      disableClose: true,
      data: this.fileTreeItem.comment
    })
      .afterClosed()
      .pipe(
        tap((result: string) => this.updateComment(result)),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  updateComment(result: string) {
    if (typeof result === 'string') {
      this.fileTreeItem.comment = result;
      this.commentChangedEvent.emit(this.fileTreeItem);
    }
  }

  deleteComment() {
    this.fileTreeItem.comment = null;
    this.commentChangedEvent.emit(this.fileTreeItem);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
