import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges
} from '@angular/core';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material';

import { FileTreeModel } from '../../../shared/models';


export interface FileTreeFlatNode {
  level: number;
  expandable: boolean;
  name: string;
  comment: string;
  state: number;
}

@Component({
  selector: 'file-tree-card',
  templateUrl: './file-tree-card.component.html',
  styleUrls: ['./file-tree-card.component.scss']
})
export class FileTreeCardComponent implements OnChanges {
  @Input() fileTree: FileTreeModel[];

  @Output() commentChangedEvent = new EventEmitter<FileTreeModel>();

  treeControl = new FlatTreeControl<FileTreeFlatNode>(
    item => item.level,
    item => item.expandable
  );

  treeFlattener = new MatTreeFlattener(
    (item: FileTreeModel, level: number) => {
      return {
        expandable: !!item.fileTree && item.fileTree.length > 0,
        name: item.name,
        comment: item.comment,
        level: level,
        id: item.id,
        state: item.state,
      };
    },
    item => item.level,
    item => item.expandable,
    item => item.fileTree
  );

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  hasChild = (_: number, item: FileTreeFlatNode) => item.expandable;

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.fileTree && changes.fileTree.currentValue && changes.fileTree.currentValue !== changes.fileTree.previousValue) {
      this.dataSource.data = this.fileTree;
    }
  }

  commentChangedHandler(fileTreeItem: FileTreeModel) {
    this.commentChangedEvent.emit(fileTreeItem);
  }
}
