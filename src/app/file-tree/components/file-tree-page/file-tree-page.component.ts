import { Component } from '@angular/core';

import { FileModel, FileTreeModel } from '../../../shared/models';
import { generateFilesFromContents } from '../../../shared/utils';
import { FileService } from '../../../core/services/file-service/file.service';
import { FileTreeService } from '../../../core/services/file-tree-service/file-tree.service';


@Component({
  selector: 'file-tree-page',
  templateUrl: './file-tree-page.component.html',
  styleUrls: ['./file-tree-page.component.scss']
})
export class FileTreePageComponent {
  files: FileModel[] = [];
  fileTree: FileTreeModel[] = [];

  get filesAlreadyExist(): boolean {
    return this.files.length > 0;
  }

  get fileListIsChanged(): boolean {
    return this.files.length > 0 && !this.files.find(item => item.state === 0);
  }

  constructor(private fileService: FileService,
              private fileTreeService: FileTreeService) {
  }

  commentChangedHandler(fileTreeItem: FileTreeModel) {
    this.fileService.updateComment(fileTreeItem, this.files);
  }

  compareFiles(fileList: FileModel[]): boolean {
    return this.files[0] && fileList[0] && this.files[0].id === fileList[0].id;
  }

  clearFileTree() {
    this.files = [];
    this.fileTree = [];
  }

  prepareFile(event: Event) {
    if (event.target['files'][0]) {
      const reader = new FileReader();
      reader.onloadend = (e) => {
        this.prepareFileContents(e.target['result']);
      };
      reader.readAsText(event.target['files'][0]);
    }
  }

  prepareFileContents(contents: string) {
    const newFileList: FileModel[] = generateFilesFromContents(contents);
    if (this.filesAlreadyExist && this.compareFiles(newFileList)) {
      this.updateFileList(newFileList);
    } else {
      this.createFileList(newFileList);
    }

    this.createFileTree();
  }

  createFileList(fileList: FileModel[]) {
    this.files = fileList.slice(0);
  }

  updateFileList(fileList: FileModel[]) {
    this.updateDefaultStates();
    this.files = this.fileService.updateFileList(this.files, fileList);
    this.checkDeletedFiles();
  }

  updateDefaultStates() {
    this.files = this.fileService.resetState(this.files);
  }

  checkDeletedFiles() {
    this.files = this.fileService.checkDeletedFiles(this.files);
  }

  createFileTree() {
    this.fileTree = this.fileTreeService.createFileTree(this.files)
  }
}
