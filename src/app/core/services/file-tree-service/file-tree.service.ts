import { Injectable } from '@angular/core';
import { FileModel, FileTreeModel } from '../../../shared/models';
import { createFileTreeChildren, sortFileTree } from '../../../shared/utils';

@Injectable({
  providedIn: 'root'
})
export class FileTreeService {

  constructor() { }

  createFileTree(files: FileModel[]): FileTreeModel[] {
    let fileTree: FileTreeModel[] = [];

    files.forEach(fileItem => {
      if (fileItem.parent === -1) {
        fileTree.push(FileTreeModel.createFileTreeItem(fileItem));
      } else {
        createFileTreeChildren(fileItem, fileTree)
      }
    });

    fileTree = sortFileTree(fileTree).slice(0);

    return fileTree;
  }
}
