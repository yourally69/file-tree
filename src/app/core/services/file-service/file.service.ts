import { Injectable } from '@angular/core';
import { FileModel, FileTreeModel } from '../../../shared/models';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor() { }

  updateFileList(fileList: FileModel[], newFileList: FileModel[]): FileModel[] {
    newFileList.forEach(newItem => {
      const item: FileModel = fileList.find(findItem => findItem.id === newItem.id);
      const index: number = fileList.indexOf(item);
      if (index >= 0) {
        fileList[index] = FileModel.updateFileItem(item, newItem);
      } else {
        newItem.state = 1;
        fileList.push(newItem);
      }
    });

    return fileList;
  }

  updateComment(item: FileTreeModel, fileList: FileModel[]) {
    const fileItem = fileList.find(item => item.id === item.id);
    const index = fileList.indexOf(fileItem);
    if (fileItem && index >= 0) {
      fileList[index].comment = item.comment;
    }
  }

  resetState(files: FileModel[]): FileModel[] {
    files.forEach(item => item.state = 0);

    return files;
  }

  checkDeletedFiles(files: FileModel[]): FileModel[]  {
    files.forEach(item => {
      if (item.state === 0) {
        item.state = 3;
      }
    });

    return files;
  }

}
