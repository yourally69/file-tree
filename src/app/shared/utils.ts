import { FileModel, FileTreeModel } from './models';

// отправляем на сортировку файловое дерево вместе с вхождениями
export function sortFileTree(fileTree: FileTreeModel[]): FileTreeModel[] {
  let newFileTree = fileTree.slice(0);
  newFileTree = _sortFileTree(newFileTree);
  newFileTree.forEach(item => {
    if (item.fileTree.length > 0) item.fileTree = sortFileTree(item.fileTree);
  });

  return newFileTree;
}

// сортировка списка
function _sortFileTree(fileTree: FileTreeModel[]): FileTreeModel[] {
  const newFileTree = fileTree.slice(0);
  newFileTree
    .sort((a, b) => _sortBy(a, b, 'name'))
    .sort((a, b) => _sortByTree(a, b));

  return newFileTree;
}

// сортировка по алфавиту
export function _sortBy(a: FileModel | FileTreeModel, b: FileModel | FileTreeModel, parameter: string): number {
  if (a[parameter] < b[parameter]) return -1;
  if (a[parameter] > b[parameter]) return 0;
  return 1;
}

// сортировка по наличию вхождения
function _sortByTree(a: FileTreeModel, b: FileTreeModel): number {
  if (a.fileTree.length > 0 && b.fileTree.length === 0) return -1;
  if (a.fileTree.length === 0 && b.fileTree.length > 0) return 0;
  return 1;
}

export function createFileTreeChildren(item: FileModel, fileTree: FileTreeModel[]) {
  const fileItem: FileTreeModel = fileTree.find(file => file.id === item.parent);
  if (fileItem) {
    fileItem.fileTree.push(FileTreeModel.createFileTreeItem(item));
  } else {
    fileTree.forEach(fileTreeItem => createFileTreeChildren(item, fileTreeItem.fileTree));
  }
}

export function generateFilesFromContents(contents: string): FileModel[] {
  let newFiles: FileModel[] = [];
  contents.split('\n').forEach(item => {
    const newFileArray: string[] = item.split(',');
    if (newFileArray[0] && newFileArray[1]) {
      newFiles.push(FileModel.createFileItem(newFileArray));
    }
  });

  newFiles.sort((a, b) => _sortBy(a, b, 'id'));

  return newFiles;
}
