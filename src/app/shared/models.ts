export class FileModel {
  id: number;
  name: string;
  state: FileStates = FileStates.Default;
  parent?: number;
  comment?: string;

  public static createFileItem(fileItem: string[]): FileModel {
    const newFileItem: FileModel = new FileModel();
    newFileItem.id = Number(fileItem[0]);
    newFileItem.name = fileItem[1];
    newFileItem.parent = Number(fileItem[2]);

    return newFileItem;
  }

  public static updateFileItem(item: FileModel, newItem: FileModel): FileModel {
    if (item.name === newItem.name && item.parent === newItem.parent) {
      item.state = 4;

      return item;
    }

    newItem.state = 2;
    newItem.comment = item.comment;

    return newItem;
  }
}

export class FileTreeModel extends FileModel {
  fileTree?: FileTreeModel[] = [];

  constructor() {
    super();
  }

  public static createFileTreeItem(fileItem: FileModel): FileTreeModel {
    const newFileTreeItem: FileTreeModel = new FileTreeModel();
    newFileTreeItem.id = fileItem.id;
    newFileTreeItem.name = fileItem.name;
    newFileTreeItem.comment = fileItem.comment;
    newFileTreeItem.state = fileItem.state;

    return newFileTreeItem;
  }
}

const enum FileStates {
  Default,
  New,
  Changed,
  Deleted,
  Double,
}
